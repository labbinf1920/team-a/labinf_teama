#!/usr/bin/env python3
''' docstring'''
from sys import argv
from ete3 import Tree, AttrFace, TreeStyle, NodeStyle

def tree_maker(newick_file):
    '''
    Esta função vai criar uma árvore filogenética e coloca-la num png
    '''

    # ler o ficheiro neewick no formato 2
    tree = Tree(newick_file, format=2)

    # mudar o tamanho e a cor da letra para cada especie
    for leaf in tree.iter_leaves():
        name = AttrFace('name', fgcolor='#1A1B4E', fsize=13)
        leaf.add_face(name, column=0)
    # mudar o estilo dos nodos e mostrar os valores de suporte na arvore
    tree_style = TreeStyle()
    tree_style.show_branch_support = True
    tree_style.show_leaf_name = False
    node_style = NodeStyle()
    node_style['shape'] = 'circle'
    node_style['size'] = 6
    node_style['fgcolor'] = '#0066cc'

    for node in tree.traverse():
        node.set_style(node_style)
    # sublinhar o ramo para a especie escolhida e mudar a cor do nodo
    species = tree.get_leaves_by_name(argv[2])[0]
    species.set_style(node_style=NodeStyle())
    species.img_style["bgcolor"] = "lightblue"
    species.img_style['shape'] = 'circle'
    species.img_style['fgcolor'] = '#c26e32'
    species.img_style["size"] = 6
    # para os irmaos da especie mudar a cor do nodo
    sisters = species.get_sisters()
    for i in range(len(sisters)):
        sisters = species.get_sisters()[i]
        sisters.set_style(node_style=NodeStyle())
        sisters.img_style['shape'] = 'circle'
        sisters.img_style['fgcolor'] = '#c26e32'
        sisters.img_style["size"] = 6
    #colocar a arvore num png
    tree_png = tree.render(argv[3] + '.png', tree_style=tree_style)

    return tree_png

if __name__ == '__main__':
    tree_maker(argv[1])
