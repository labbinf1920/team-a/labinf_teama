#!/usr/bin/python3

'''Import all the libraries needed'''
from sys import argv
# import os
import glob
import json
from Bio.Align.Applications import MafftCommandline

def get_species(taxonomy_dict_path):
    '''Reads json file from taxonomy step and returns a list of all the species in it'''

    # Open and load the json file
    with open(taxonomy_dict_path, 'r') as taxonomy_file:
        taxonomy_dict = json.load(taxonomy_file)

    # Assumes keys of dictionary  are species names, cleans it, and returns it
    species_names = [species.split(" (")[0] for species in taxonomy_dict]
    return species_names


def align_seqs(work_dir):

    '''Align all the sequence files and put them all together in a new one file '''
    # print(os.getcwd())
    # print(work_dir + '*.fasta')
    file_list = glob.glob(work_dir + '*.fasta')
    # print("\n \n These are the files in the \"genes\" folder:\n \n", file_list, "\n \n")

    for file_item in file_list:

        #Do the alignment of the sequences

        mafft_exe = '/opt/conda/bin/mafft'
        input_file = file_item
        mafft_cline = MafftCommandline(mafft_exe, input=input_file)

        stdout, _stderr = mafft_cline()

        # Clean names:
        taxa = stdout.split(">")

        for taxon in taxa:
            taxon = taxon.split("\n")
            header = taxon[0]
            sequence = taxon[1:]
            sequence = [a for a in sequence if a != " "]
            sequence = "".join(sequence)
            for specie in SPECIES_LIST:
                if specie in header:
                    header = ">" + specie
                    this_specie = specie
                    SPECIES_DICT[this_specie] = SPECIES_DICT[this_specie] + sequence

            # print("HEADER: " + header)
            # print("SEQUENCE: " + "".join(sequence))

def write_all(taxa_dictionary):
    ''' Takes a dictionary where the keys are the names of species
        and their respective values are the aligned genes concatenated '''

        # Create a new file and copy all the "new information"
    with open("all_new_files.fasta", "w") as handle:
        for species, algn_sequences in taxa_dictionary.items():
            if algn_sequences != "":
                handle.write("\n>" + species + "\n" + algn_sequences)
        handle.close()

if __name__ == '__main__':
    WORK_DIR = argv[1]
    SPECIES_LIST = get_species(argv[2])
    SPECIES_DICT = dict.fromkeys(SPECIES_LIST, "")
    align_seqs(WORK_DIR)
    write_all(SPECIES_DICT)
    # print(SPECIES_DICT)
    # print(len(SPECIES_DICT))
