#alinha as sequencias seguindo a ideologia seguinte (adequado para sequências de comprimentos semelhantes; método de refinamento iterativo que incorpora informações globais de alinhamento por pares): 	
# mafft --localpair --maxiterate 1000 --seed "$1" > sequencias_alinhadas "$1"
# linsi --clustalout "$1" 

#vai ler o fasta e retirar a sequecia do gene presente
#$1 corresponde ao ficheiro com as sequecias alinhadas,$2 corresponde ao numero de cores utlilizados, o $3 corresponde ao numero de bootstraps efetuados, $4 imprime o processo.

raxmlHPC-PTHREADS-AVX -T "$2" -f a -m GTRCAT -p 513846 -x 513846 -N "$3" -s "$1" -n Test

echo "$4"


