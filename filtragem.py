#!/usr/bin/env python3
'''
This program will open a dictionary in JSON format.
It will also process this file and insert into a table, which it will have as columns the Genes,
as rows Names and the result of the interception will be the corresponding ID.
This ID will be used afterwards in the NCBI to retrieve the accession number of the gene of the
specie to use to obtain his corresponding sequence.

inputs:
    myfile => JSON file of a dictionary containing the name of the specie as keys and another
    dictionary as values,
corresponding to the gene and respective ID
     igualdade => It's the percentage of equality the user wants

outputs:
    A bunch of fasta files of each gene in each species

'''

# imports
import os
import sys
import json
import pandas as pd
from tabulate import tabulate
from taxon_finder import searcher


def abrir_ficheiro(infile):
    '''
    Open a JSON file and read it
    :return:
        A JSON file containing all accessions, species names and genes
    '''
    with open(infile, 'r') as myfile:
        ficheiro = json.load(myfile)
    return ficheiro


def converter_dataframes(f_json):
    '''
    Convert list of tuples to dataframe and set column, names and indexes
    '''
    dataframe = pd.DataFrame(f_json)
    return dataframe


def contagem_nome(lista):
    '''
    This function will count how many times the specie name appears
    '''
    dicionario = {}
    for linha in lista:
        dicionario[linha] = dicionario.get(linha, 0) + 1

    frequencia_da_palavra = []
    for chave, valor in dicionario.items():
        frequencia_da_palavra.append((valor, chave))

    return dicionario


def construcao_tabela(data, especie):
    '''
    This function will construct a table.
        The rows will be the gene,
    	The columns will be the name of the species
            The interaction is a number
    '''

    tabela = tabulate(data, headers=especie.keys(), showindex='always', tablefmt="github",
                      numalign="left", colalign=("center",), floatfmt=".0f")
    # print(tabela)

    return tabela


def remover_nulos(valores):
    '''
    This function will remove every "nan" in the list
    :return:
        One list
    '''
    lista = []
    for posicao in valores:
        if isinstance(posicao, str) is True:
            lista.append(posicao)

    return lista


def percentagem(dataframe):
    '''
    This function will ask what percentage will the user want
    :return:
        One list without the nulls
    '''
    transpose = dataframe.T
    igualdade = int(sys.argv[1]) / 100
    especie = len(dataframe.keys())
    accessions_raw = []
    genes = []
    for chave in enumerate(transpose.keys()):
        temp = transpose[chave[1]].count() / especie
        if temp >= igualdade:
            genes.append(chave[1])
            valores = transpose[chave[1]].values
            accessions_raw.append(remover_nulos(valores))
    return accessions_raw, genes


def separador_de_indexes_accessions(lista):
    '''
    This function separate the indexes from accession numbers.
    param lista:
        Accepts a list of accessions and genes as input
    :return:
        A list of accessions only
    '''
    temp_list = []
    accessions_treated = []
    for tuplo in enumerate(lista):
        for accession in enumerate(tuplo[1]):
            temp = lista[tuplo[0]][accession[0]].split(" ")
            temp[1] = temp[1][1:-1]
            temp[1] = temp[1].split("..")
            temp_list.append(temp)
        accessions_treated.append(temp_list)
        temp_list = []
    return accessions_treated


def busca_sequencias(lista_accessions):
    '''
    This function will  get the sequences
    :params lista_accessions:
        Accepts a list of accession numbers as input
    :return:
        A list of sequences
    '''
    sequencias = []

    for gene in lista_accessions:
        for acc_idx in gene:
            if "complement)" in acc_idx:
                acc_idx = acc_idx[:-1]

            number = acc_idx[0]
            inicio = acc_idx[1][0]
            fim = acc_idx[1][1]

            termo = number + "&seq_start=" + inicio + "&seq_stop=" + fim
            seq = searcher(termo, "nuccore", "fasta")
            sequencias.append(seq.text)
    return sequencias


def cria_ficheiro(lista_sequencias, lista_gene, lens):
    '''
    This function will create a folder "genes" if it doesn't
    exist and create some files with the genes
    :params lista_sequencias,lista_gene,lens:
    	Receives 3 lists to write in a file.fasta, for each genes
    '''
    os.makedirs("genes", exist_ok=True)
    for gene in enumerate(lista_gene):
        titulo = gene[1] + ".fasta"
        nome = ""
        with open("genes/" + titulo, 'a') as file_handler:
            for nome in range(lens[gene[0]]):
                file_handler.write(lista_sequencias[nome])
            lista_sequencias = lista_sequencias[nome + 1:]
        file_handler.close()


if __name__ == "__main__":
    ABERTURA_DE_FICHEIRO = abrir_ficheiro('DICTI.json')
    DATAF = converter_dataframes(ABERTURA_DE_FICHEIRO)
    NOME = contagem_nome(DATAF)
    TABLE = construcao_tabela(DATAF, NOME)
    ACCESIONS, GENES = percentagem(DATAF)
    ACCESIONS = separador_de_indexes_accessions(ACCESIONS)
    LENS = []
    for i in ACCESIONS:
        LENS.append(len(i))
    SEQUENCIAS = busca_sequencias(ACCESIONS)
    cria_ficheiro(SEQUENCIAS, GENES, LENS)
