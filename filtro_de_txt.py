#!/usr/bin/env python3
'''
Este programa realiza a filtragem de um ficheiro recebido(ficheiro).
Irá retirar o nome, ID e genes de um fiheiro para poder ser trabalhado
'''


def seletor_de_genes_nomes_e_id(ficheiro):
    '''
    Esta função irá receber um parametro 'ficheiro'
    será retirado daqui o nome da especie, o gene e o ID, tendo que retornar  a 'linha'.
    '''

    lista_gene = []
    lista_nome = []
    lista_id = []

    with open(ficheiro) as file_handler:
        line_counter = 1
        for line in file_handler:
            if line == "\n":
                line_counter = 1
            else:
                if line_counter == 1:
                    # irá procurar nas linhas 1 e 2 o gene e o nome da especie, respetivamente
                    lista_gene.append(line[3:-1])
                    line_counter += 1
                elif line_counter == 2:
                    inicio_string = (line.find(' [') + 2)
                    fim_string = (line.find(']'))
                    lista_nome.append(line[inicio_string:fim_string])
                    line_counter += 1

                elif line.startswith("Annotation: "):
                    # procura neste bloco o 'Annotation: '
                    # irá obter um ID relativo ao gene e especie
                    lista_id.append(line[line.find("NC_"):])
                    line_counter += 1
                else:
                    continue
                if line_counter == 4:
                    if len(lista_gene) > len(lista_nome):
                        lista_gene.pop()
                    elif len(lista_gene) < len(lista_id):
                        lista_id.pop()
                    elif len(lista_nome) > len(lista_id):
                        lista_nome.pop()
        return lista_gene, lista_nome, lista_id
