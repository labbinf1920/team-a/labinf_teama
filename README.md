# LabInf TEAM A

## Construção de Árvores Filogenéticas através de API's públicas (NCBI/entrez)

### Processo:

* Receber info do taxa inicial e o nível da "classe"
* Download do taxa Inicial
* Criar lista das espécias da classe
* Alinhamento
* Reconstrução:
    * 1-ML
    * 2-Mr.Bayes:
        * Conversão de fasta para Nexus
    * 3-ML + Mr.Bayes:
* Conversão de fasta para Nexus
* Construção das árvores


### Requesitos:
* Instalação do Docker

### Parâmetros aceites:
* oGSpecie = espécie em estudo (default: "Passer domesticus")
* fen_lvl = nível de relação (deafult: "family") 
* gene_completeness: percentagem de vezes que um gene tem que aparecer entre as espécies para ser considerado.
* tree = tipo de reconstrução (1- ML 2-Mr.Bayes 3- ML + Mr.Bayes (default: 3))
* cores = número de cores a usar (default: 2)
* ngen= número de gerações no Mr.B (default: 1 mil)
* bootstrap= número de bootstraps em ML (default: 1000)


### Como utilizar:

Realizar download/update da imagem: 
`sudo docker pull thomasmelo/grupoafinal:latest`

Iniciar a imagem com um volume (i.e.):
`docker run -v $HOME/Desktop/Snakerun/:/tmp/out -i -t  thomasmelo/grupoafinal:latest`

Executar o Snakemake:
`snakemake --config bootstrap=1000 ngen= 1000`

Nota: No exemplo da execução da imagem, uma pasta é criada no Desktop com o nome "Snakerun". Nesta pasta chegaram 2 ficheiros após a execução. Um zip com os ficheiros gerados ao longo da mesma, e o(s) .png resultantes da reconstrução.
