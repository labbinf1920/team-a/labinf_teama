#!/usr/bin/env python3
'''
Este programa vai criar uma arvore filogenetica do mr.bayes.
O programa vai abrir um ficheiro nexus que e transformado num ficheiro newick,
que sera utilizado para fazer a arvore filogenetica com o ete3.
o output da arvore vai conter a especie escolhida com cor diferente e
os nodos dos ramos irmaos vao estar a cor laranja.

input:
    nexus_file = ficheiro .con.tre com a arvore do mr.bayes.

    argv[1] -> ficheiro_da_arvore.con.tre
    argv[2] -> nome da especie que quer sublinhar

output:
    mb_tree.png = Imagem png com o desenho da arvore.

'''

#imports
from sys import argv
import re
import dendropy
from ete3 import Tree


def nexus_newick(nexus_file):

    '''Esta funcao recebe um ficheiro .con.tre e converte-o para newick'''

    # converter o nexus para newick
    nexus_converter = dendropy.Tree.get(path=nexus_file, schema='nexus')
    tree_string = nexus_converter.as_string(schema='newick', suppress_rooting=False, \
suppress_annotations=False, annotations_as_nhx=True)

    # retirar todos os caracteres que nao sejam os valores de probabilidade
    tree_string = re.sub('prob=.+?(?=prob\\(percent\\))', '', tree_string)
    tree_string = re.sub('(?<=prob\\(percent\\)=[0-9][0-9][0-9]):.\
*?(?=\\])|(?<=prob\\(percent\\)=[0-9][0-9]):.*?(?=\\])', '', tree_string)
    tree_string = re.sub('\\(percent\\)', '', tree_string)
    tree_string = re.sub('\\[&U]', '', tree_string)# eliminar caracter que impede a leitura no ete3
    # procurar os valores de probabilidade e coloca-los numa lista
    lista = re.findall('\\):(.*?)\\]', tree_string)
    probabilidades = []
    for word in lista:
        word = re.sub('(.*?)\\[&&NHX:prob=', '', word)
        probabilidades.append(')'+word+':')

    # converter o ficheiro newick da arvore para um formato com valores de probabilidade (format 2)
    tree_file = Tree(tree_string)
    tree_file = tree_file.write(format=2)

    # substituir cada valor de probabilidade por um valor de probabilidade da lista
    for i in enumerate(probabilidades, start=0):
        tree_file = re.sub('\\)1:', probabilidades[i[0]], tree_file, 1)
    # guardar as mudanças num ficheiro novo
    newick = open('newick_tree.nwk', 'w')
    newick.write(tree_file)
    newick.close()

    return newick

if __name__ == '__main__':
    nexus_newick(argv[1])
