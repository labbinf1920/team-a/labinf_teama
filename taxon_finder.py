#!/usr/bin/env python3

"""
Cria uma query a partir da espécie pedida e retorna um ficheiro json da API do entrez.
input: string contendo o nome de uma espécie a pesquisar e filogenia desejada (Familia, Genero, etc)
output: Ficheiro json contendo um dicionario com:
    -o nome de cada especie,
    -os genes
    -accession numbers
"""

# imports
import sys
import xml.etree.ElementTree as ET
import tempfile
import json
import requests as req
from filtro_de_txt import seletor_de_genes_nomes_e_id as seletor


def searcher(term, database, rettype):
    """
    Given the arguments "term", "databse" and type it retrieves a requests.get object
    """

    # base vars

    search = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?"
    fetch = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?"
    database = "db=" + database
    search_term = "&term=" + term

    if rettype == "text":
        ret = "&retmode=text"
    else:
        ret = "&rettype=" + rettype

    # First request to get Web_env and Query key

    info = req.get(search + database + search_term + "&usehistory=y")
    info = info.text

    # Locates Wev Env and query key

    web_env = "&WebEnv=" + info[info.find("<WebEnv>") + 8:info.find("</WebEnv")]
    query_key = "&query_key=" + info[info.find("<QueryKey>") + 10:info.find("</QueryKey>")]

    # Final request that retrives what we want
    if "seq_" in search_term:
        indexes = search_term[search_term.find("&seq"):]
        page = req.get(fetch + database + web_env + query_key + indexes + ret)
    else:
        page = req.get(fetch + database + web_env + query_key + ret)
    return page


def fetcher_ids_names_genes(filename, position):
    '''
    With a taxonomy file and a position it returns the id, name and rank given
    '''
    root = ET.fromstring(filename.text)
    ids = []
    name = []
    rank = []
    family = ""
    for i in root.iter("Taxon"):
        ids.append(i.find("TaxId").text)
        name.append(i.find("ScientificName").text)
        rank.append(i.find("Rank").text)
        if i.find("Rank").text == position:
            family = [i.find("TaxId").text, i.find("ScientificName").text, i.find("Rank").text]
    return family


def file_creator(file_raw):
    '''
    From the gene table of requests object it will create a tempfile
    '''

    filetemp = tempfile.NamedTemporaryFile()
    temp_name = filetemp.name
    filetemp.close()
    with open(temp_name, 'w') as file_handler:
        for i in file_raw:
            file_handler.write(i)
            file_handler.write("\n")
    file_handler.close()
    return filetemp


def gene_table_converter_to_lists(gene_list):
    '''
    Given a gene table file, it returns 3 lists containing:
        the genes of the given taxon
        the names of the species,
        the ids of the species with the specific gene
    EX:
        ID:33912058
        Name: ADH dehydrogenase subunit 2 [Prunella fulvescens (Brown accentor)]
        Gene: ND2
    '''

    lista_genes = []
    lista_nomes = []
    lista_ids = []

    for i in gene_list:
        if ". " in i:
            lista_genes.append(i[i.find(". ") + 2:].upper())
        elif "[" in i:
            lista_nomes.append(i[i.find(" [") + 2:i.find("]")])
        elif "ID: " in i:
            lista_ids.append(i[i.find("ID:") + 3:])
    return lista_genes, lista_nomes, lista_ids


def taxon_dict(genes, nomes, ids):
    '''
    Given three lists containing genes, names and accessions of the species,
    it creates a dictionary with the format:
    {"Species1":{"Gene1":Accession1:},{"Species2":{"Gene1":Accession2,"Gene2":Accession3} ... }
    '''

    # mega_lista = [{nomes[i]: {genes[i]: ids[i]}} for i in range(len(nomes))]
    mega_lista = []
    # print("Nome: %s | Gene: %s | ID: %s" % (len(nomes),len(genes),len(ids)))
    try:
        for i in enumerate(nomes):
            # print("Volta: %s |Nome: %s | Gene: %s | ID: %s" % (i, nomes[i], genes[i], ids[i]) )
            mega_lista.append({nomes[i[0]]: {genes[i[0]]: ids[i[0]]}})
        dictionary = {i: {} for i in nomes}

        for i in enumerate(mega_lista):
            for key, value in mega_lista[i[0]].items():
                dictionary[key].update(value)
    except IndexError:
        print("ERRO! O termo inserido não contém genes associados na base de dados \'Gene\'."
              "Por favor tente com outro termo ou generalizar a espécie. ")
        sys.exit(1)
    return dictionary


if __name__ == '__main__':
    SPECIES = sys.argv[1]
    FEN_LVL = sys.argv[2]
    FILENAME = searcher(SPECIES, "taxonomy", "xml")
    LINHAGEM = fetcher_ids_names_genes(FILENAME, FEN_LVL)
    TAXON_ID = "txid" + LINHAGEM[0] + "[Organism:Exp]"
    GENE_LIST = searcher(TAXON_ID, "gene", "text").text.split("\n")
    GENE_FILE = file_creator(GENE_LIST)
    GENES, NOMES, IDS = seletor(GENE_FILE.name)
    for gene in GENES:
        GENES = [gene.replace(".", "") for gene in GENES]
        GENES = [gene.replace(" ", "") for gene in GENES]
    SPECIES_TABLE = taxon_dict(GENES, NOMES, IDS)
    for specie in SPECIES_TABLE:
        for gene in SPECIES_TABLE[specie]:
            SPECIES_TABLE[specie][gene] = SPECIES_TABLE[specie][gene].replace("\n", "")

    # Added output file to simplify usage on pipeline
    with open('DICTI.json', 'w') as fp:
        json.dump(SPECIES_TABLE, fp)
