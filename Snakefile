import os
import glob
import re

singularity: "docker://thomasmelo/second:latest"
configfile: "config.json"

# Requires more conditions to verify values!

def build_log():
        log = "Queried Specie: " + config["oGSpecie"] + "\t(default is Passer domesticus)"
        log += "\nReationship Level: " + config["fen_lvl"] + "\t\t\t(default is family)" # ADD CONDITIONS
        if config["gene_completeness"] > 0 and config["gene_completeness"] <= 100:
            log += "\nGene \"Completeness\": " + str(config["gene_completeness"]) + "\t\t\t\t(default is 20)"
        else:
            raise ValueError('Bad "gene_completeness" value. It must be between [1-100]')
        if config["tree"] == 1:
            log += "\nTree Model: ML trough RAxML (" + str(config["tree"]) + ")"
        elif config["tree"] == 2:
            log += "\nTree Model: Mr.Bayes (" + str(config["tree"]) + ")"
        elif config["tree"] == 3:
            log += "\nTree Model: ML trough RAxML + Mr.Bayes (" + str(config["tree"]) + ") \t(default is RAxML + Mr.Bayes)"
        else:
            raise ValueError('Bad Tree Value. It must be "1" for ML (RAxML) "2" for Mr.Bayes and "3" for both.')
        if config["cores"] < 2:
            raise ValueError("The minimum number of cores is 2")
        else:
            log += "\nCores: " + str(config["cores"]) + "\t\t\t\t\t(default is 2)"
        if config["ngen"] < 0:
            raise ValueError("ngen value must be > 1!")
        else:
            log += "\nNgen: " + str(config["ngen"]) + "\t\t\t\t\t(default is 1000000)"
        if config["bootstrap"] < 10:
            raise ValueError("bootstrap value must be >= 10!")
        else:
            log += "\nBootstraps: " + str(config["bootstrap"]) + "\t\t\t\t(default is 1000)"

        # Print the log and save it
        print(log)

        with open("run_parameters.txt" , "w") as start_log:
            start_log.write(log)

rule all:
    input:
        'run_files.zip'

    message:
        'Pipeline ran successfully!\n\nAll processing files and logs are in "run_files.zip".'

rule test_input:
    input:
        'config.json'

    output:
        'run_parameters.txt'
        
    message:
        'Parameters passed saved to > "run_parameters.txt"' 

    run:
        build_log()


rule taxonomy:
    input:
        'run_parameters.txt'

    output:
        'DICTI.json'

    message:
        'Preparing taxonomy file "DICTI.json"'

    shell:
        'python3 taxon_finder.py "{config[oGSpecie]}" "{config[fen_lvl]}"'


rule filter:
    input:
        'DICTI.json'

    message:
        'Getting genes'

    output:
        dynamic('genes/{fastas}.fasta')

    shell:
        'python3 filtragem.py "{config[gene_completeness]}"'


rule align:
    input:
        dynamic('genes/{fastas}.fasta')

    message:
        'Aligning and aggreagating sequences'

    output:
        'all_new_files.fasta'

    shell:
        'python3 alignment.py $PWD/genes/ $PWD/DICTI.json'



rule clean_fasta:
    input:
        'all_new_files.fasta'
    message:
        'Removing possible invalid characters from aligned fasta file'
    output:
        'aligned_clean.fasta'
    shell:
        'sed "s/[;|:|,]/_/g" all_new_files.fasta > aligned_clean.fasta'


rule build_tree:
    input:
        'aligned_clean.fasta'
    output:
        'build_tree_log.txt'
    params:
        tree = config['tree'],
        cores = config['cores'],
        bootstrap = config['bootstrap'],
        ngen = config['ngen'],
        specie = config['oGSpecie'].replace(" ", "_")
    run:
        if params.tree == 1:      # ML
            # Remove whitespace from sequence names (avoid RAxML error)
            shell("sed 's, ,_,g' -i aligned_clean.fasta")
            shell("./scriptraxml.sh aligned_clean.fasta {params.cores} {params.bootstrap} > build_tree_log.txt")
            shell("xvfb-run python3 tree_maker.py RAxML_bipartitions.Test{params.specie} {params.specie}_ml_tree")

        elif params.tree == 2:    # Mr.Bayes (fasta -> nexus)
            shell("echo {params.specie}")
            # Convert fasta to nexus
            # Remove whitespace from sequence names
            shell("sed 's, ,_,g' -i aligned_clean.fasta")
            shell("./fasta_nexus.sh aligned_clean.fasta {params.ngen}")
            shell("./mrbayes.sh nexus_BrBayes.nex  > build_tree_log.txt")
            shell("xvfb-run python3 mb_tree_pl.py MyRun01.con.tre")
            shell("xvfb-run python3 tree_maker.py newick_tree.nwk {params.specie} {params.specie}_mb_tree")

        elif params.tree == 3:    # ML + Mr.Bayes (fasta -> nexus)
            # Remove whitespace from sequence names (avoid RAxML error)
            shell("sed 's, ,_,g' -i aligned_clean.fasta")
            shell("./scriptraxml.sh aligned_clean.fasta {params.cores} {params.bootstrap} > build_tree_log.txt")
            shell("xvfb-run python3 tree_maker.py RAxML_bipartitions.Test {params.specie} {params.specie}_ml_tree")
            shell("./fasta_nexus.sh aligned_clean.fasta {params.ngen}")
            shell("./mrbayes.sh nexus_BrBayes.nex  > build_tree_log.txt")
            shell("xvfb-run python3 mb_tree_pl.py MyRun01.con.tre")
            shell("xvfb-run python3 tree_maker.py newick_tree.nwk {params.specie} {params.specie}_mb_tree")

rule zip:
  input:
      'build_tree_log.txt'
  output:
      'run_files.zip'
  run:
      shell("zip -m run_files *.fasta MyRun* RAxML* *.txt DICTI.json *.newick *.nex *.nwk")
      shell("zip -m -r run_files genes")
      shell("mkdir -p out")
      shell("cp run_files.zip out/run_files.zip")
      shell("mv *.png out")
