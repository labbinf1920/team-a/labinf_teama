seqmagick convert --output-format nexus --alphabet dna "$1" "nexus_BrBayes".nex #entra um ficheiro fasta ($1)

echo "begin mrbayes;
  set autoclose=yes;
  mcmcp ngen="$2" printfreq=1000 samplefreq=100 diagnfreq=1000 nchains=4 savebrlens=yes filename= MyRun01;
  mcmc;
  sumt filename = MyRun01;
end;" >> "nexus_BrBayes".nex

#$1_ficheiro fasta
#$2 ngen é nos fornecido. 

#para correr na linha de comandos basta fazer os seguintes passos
	# chomod +x ./fasta_nexus.sh
	#./fasta_nexus.sh "$1" "$2" 

