 #Download base image ubuntu 16.04
FROM continuumio/miniconda3

RUN apt-get update
RUN conda install -c etetoolkit ete3
RUN export PATH=~/anaconda_ete/bin:$PATH
RUN conda install -c bioconda -c conda-forge snakemake
RUN pip install seqmagick
RUN python3 -m pip install git+https://github.com/jeetsukumaran/DendroPy.git
RUN pip install requests
RUN pip install pandas
RUN pip install tabulate
RUN pip install biopython
RUN conda install -c bioconda mafft
RUN apt-get install raxml -y
RUN apt-get install mrbayes -y
RUN apt-get install libxi6 libgconf-2-4 -y
RUN apt-get install xvfb -y
RUN apt-get install zip unzip

# GET into snakemake's tmp folder
RUN cd tmp

# Pass the required scripts inside the container
COPY *.py tmp/
COPY *sh tmp/
COPY Snakefile tmp/
COPY config.json tmp/

# Change directory on boot
WORKDIR /tmp
